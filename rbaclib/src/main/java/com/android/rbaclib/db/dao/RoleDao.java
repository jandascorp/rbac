package com.android.rbaclib.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import com.android.rbaclib.db.enitity.Role;

@Dao
public interface RoleDao {

    @Insert
    long insert(Role role);

}
