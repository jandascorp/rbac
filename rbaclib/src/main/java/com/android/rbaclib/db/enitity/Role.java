package com.android.rbaclib.db.enitity;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(
        tableName = "roles",
        indices = {@Index(value = "name", unique = true)}
)
public class Role implements Serializable {

    public void setName(String name) {
        this.name = name;
    }

    @PrimaryKey(autoGenerate = true)
    private int role_id;

    private String name;

    public Role(String name) {
        this.name = name;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + role_id +
                ", name='" + name + '\'' +
                '}';
    }
}
