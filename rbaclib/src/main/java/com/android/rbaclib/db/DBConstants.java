package com.android.rbaclib.db;

public class DBConstants {
    public static final String DB_NAME = "rbac";
    public static final String TBL_USERS = "users";
    public static final String TBL_ROLES = "roles";
    public static final String TBL_WEBSITES = "websites";
}
