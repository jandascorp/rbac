package com.android.rbaclib.db.enitity;

import androidx.room.Embedded;
import androidx.room.Junction;
import androidx.room.Relation;

import java.util.List;

/**
 * Entity table for querying permissions.
 */
public class RoleWithPermissions {
    @Embedded
    public Role role;
    @Relation(
            parentColumn = "role_id",
            entityColumn = "website_id",
            associateBy = @Junction(Permissions.class)
    )
    public List<Website> websites;

    @Override
    public String toString() {
        return "{" +
                "role=" + role +
                ",websites=" + websites +
                "}\n";
    }
}
