package com.android.rbaclib.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.android.rbaclib.db.dao.RoleDao;
import com.android.rbaclib.db.dao.RolePermissionJoinDao;
import com.android.rbaclib.db.dao.UserDao;
import com.android.rbaclib.db.dao.WebsiteDao;
import com.android.rbaclib.db.enitity.Permissions;
import com.android.rbaclib.db.enitity.Role;
import com.android.rbaclib.db.enitity.User;
import com.android.rbaclib.db.enitity.Website;


@Database(entities = {Role.class, User.class, Website.class, Permissions.class}, version = 1)
public abstract class RABCDatabase extends RoomDatabase {

    public abstract UserDao getUserDao();
    public abstract RoleDao getRoleDao();
    public abstract WebsiteDao getWebsiteDao();
    public abstract RolePermissionJoinDao getRolePermissionJoinDao();

    private static RABCDatabase noteDB;

    public static RABCDatabase getInstance(Context context) {
        if (null == noteDB) {
            noteDB = buildDatabaseInstance(context);
        }
        return noteDB;
    }

    private static RABCDatabase buildDatabaseInstance(Context context) {
        return Room.databaseBuilder(context,
                RABCDatabase.class,
                DBConstants.DB_NAME)
                .allowMainThreadQueries().build();
    }

    public void cleanUp() {
        noteDB = null;
    }

}