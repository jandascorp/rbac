package com.android.rbaclib.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.android.rbaclib.db.enitity.Permissions;
import com.android.rbaclib.db.enitity.RoleWithPermissions;

import java.util.List;
@Dao
public interface RolePermissionJoinDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertRoleWithPermission(Permissions... permissions);

    @Transaction
    @Query("SELECT * FROM roles")
    List<RoleWithPermissions> getRoleWithPermissions();
}
