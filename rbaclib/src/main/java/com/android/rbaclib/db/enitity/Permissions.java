package com.android.rbaclib.db.enitity;

import androidx.room.Entity;
import androidx.room.ForeignKey;

/**
 * Associate Entity table holding many to many relation ship between role and websites
 */
@Entity(tableName = "role_permission_join", primaryKeys = {"role_id", "website_id"})
public class Permissions {
    public int role_id;
    public int website_id;

    public Permissions(int role_id, int website_id) {
        this.role_id = role_id;
        this.website_id = website_id;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + role_id +
                ", website_id=" + website_id +
                "}\n";
    }
}
