package com.android.rbaclib.db.dao;

import androidx.room.Dao;
import androidx.room.Insert;

import com.android.rbaclib.db.enitity.Website;

@Dao
public interface WebsiteDao {

    @Insert
    long insert(Website website);
}
