package com.android.rbaclib.db.enitity;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


import java.io.Serializable;

@Entity(tableName = "websites", indices = {@Index(value = {"url"}, unique = true)})
public class Website implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int website_id;

    private String url;

    public Website(String url){
        this.url = url;
    }

    public int getWebsite_id() {
        return website_id;
    }

    public void setWebsite_id(int website_id) {
        this.website_id = website_id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + website_id +
                ", url='" + url + '\'' +
                '}';
    }
}
