package com.android.rbaclib.db.enitity;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.android.rbaclib.db.DBConstants;

import java.io.Serializable;

@Entity(tableName = "users")
public class User implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id = 0;

    private String name;

    private int role_id;

    public User(String name, int role_id) {
        this.name = name;
        this.role_id = role_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id){
        this.id=id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRole_id() {
        return role_id;
    }

    public void setRole_id(int role_id) {
        this.role_id = role_id;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ",\n name='" + name + '\'' +
                ",\n role_id=" + role_id +
                "}\n";
    }
}