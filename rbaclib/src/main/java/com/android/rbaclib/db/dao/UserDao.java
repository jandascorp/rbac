package com.android.rbaclib.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.android.rbaclib.db.enitity.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("SELECT * FROM users")
    List<User> getAll();

    @Query("SELECT * FROM users WHERE name LIKE '%' || :nameString  || '%'")
    List<User> search(String nameString);

    @Insert
    long insert(User user);

    @Delete
    void delete(User user);

    @Query("DELETE FROM users WHERE id=:userId;")
    int delete(int userId);


    @Query("SELECT EXISTS(SELECT websites.website_id, websites.url FROM websites INNER JOIN role_permission_join ON (websites.website_id = role_permission_join.website_id)" +
            " WHERE (role_permission_join.role_id IN (SELECT u.role_id FROM users u WHERE u.name = :user)" +
            " AND websites.website_id= :websiteId))")
    boolean hasAccessToWebsite(String user, int websiteId);

    @Query("SELECT EXISTS(SELECT websites.website_id, websites.url FROM websites INNER JOIN role_permission_join ON (websites.website_id = role_permission_join.website_id)" +
            " WHERE (role_permission_join.role_id IN (SELECT u.role_id FROM users u WHERE u.name = :user)" +
            " AND websites.url= :website))")
    boolean hasAccessToWebsite(String user, String website);

}