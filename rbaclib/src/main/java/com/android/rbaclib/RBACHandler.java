package com.android.rbaclib;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.android.rbaclib.db.RABCDatabase;
import com.android.rbaclib.db.enitity.Permissions;
import com.android.rbaclib.db.enitity.Role;
import com.android.rbaclib.db.enitity.RoleWithPermissions;
import com.android.rbaclib.db.enitity.User;
import com.android.rbaclib.db.enitity.Website;

import java.util.List;

public class RBACHandler {

    private static RBACHandler instance;
    private static RABCDatabase dbInstance;
    private static final String TAG = RBACHandler.class.getName();

    public static RBACHandler getInstance(Context context) {
        if (instance == null) {
            instance = new RBACHandler();
            dbInstance = RABCDatabase.getInstance(context);
            initDbData(context);
        }
        return instance;
    }

    private static void initDbData(Context context) {
        /**
         * check if database default values are initialized
         * if not initialize the database data and make flag true
         */
        SharedPreferences prefs = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        Boolean initialized = prefs.getBoolean("initialized", false);
        if (!initialized) {

            Log.d(TAG, "\n\n\n");
            Log.d(TAG, "Initializing database default values for roles and websites");
            String[] roles = {"ADMIN", "CUSTOMER 1", "CUSTOMER 2"};
            String[] websites = {
                    "website1.com",
                    "website2.com",
                    "website3.com",
                    "website4.com"
            };

            Log.d(TAG, "\n\n\n");
            Log.d(TAG, "Creating Roles");
            String roleLog = "%s Role created with id : %d";
            int roleAdmin = (int) dbInstance.getRoleDao().insert(new Role(roles[0]));
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format(roleLog, roles[0], roleAdmin));
            int roleCustomer1 = (int) dbInstance.getRoleDao().insert(new Role(roles[1]));
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format(roleLog, roles[1], roleCustomer1));
            int roleCustomer2 = (int) dbInstance.getRoleDao().insert(new Role(roles[2]));
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format(roleLog, roles[2], roleCustomer2));

            Log.d(TAG, "Creating Websites");
            String websiteLog = "%s created with id : %d";
            int website1 = (int) dbInstance.getWebsiteDao().insert(new Website(websites[0]));
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format(websiteLog, websites[0], website1));
            int website2 = (int) dbInstance.getWebsiteDao().insert(new Website(websites[1]));
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format(websiteLog, websites[1], website2));
            int website3 = (int) dbInstance.getWebsiteDao().insert(new Website(websites[2]));
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format(websiteLog, websites[2], website3));

            Log.d(TAG, "\n\n\n");
            Log.d(TAG, "Creating Permissions:");
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format("%s has access to %s, %s, %s", roles[0], websites[0], websites[1], websites[2]));
            dbInstance.getRolePermissionJoinDao().insertRoleWithPermission(
                    new Permissions(roleAdmin, website1),
                    new Permissions(roleAdmin, website2),
                    new Permissions(roleAdmin, website3)
            );
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format("%s has access to %s, %s", roles[1], websites[0], websites[1]));
            dbInstance.getRolePermissionJoinDao().insertRoleWithPermission(
                    new Permissions(roleCustomer1, website1),
                    new Permissions(roleCustomer1, website2)
            );
            Log.d(TAG, "\n\n\n");
            Log.d(TAG, String.format("%s has access to %s", roles[2], websites[2]));
            dbInstance.getRolePermissionJoinDao().insertRoleWithPermission(
                    new Permissions(roleCustomer2, website3)
            );

            prefs.edit().putBoolean("initialized", true).apply();
        } else {
            instance.allUsers().toString();
            instance.listRolesWithPermissions().toString();
        }
    }

    public List<RoleWithPermissions> listRolesWithPermissions() {
        Log.d(TAG, "Listing Roles with permissions");
        Log.d(TAG, "\n\n\n");
        List<RoleWithPermissions> allRoles = dbInstance.getRolePermissionJoinDao().getRoleWithPermissions();
        Log.d(TAG, allRoles.toString());
        return allRoles;
    }

    public void addUser(String name, int role_id) {
        /**
         * Add user with role passed
         */
        long userId = dbInstance.getUserDao().insert(new User(name, role_id));
        Log.d(TAG, String.format("Added New User : %s with role %s", name, role_id));

    }

    public List<User> allUsers() {
        /**
         * Returns list of all users
         */
        List<User> all_users = dbInstance.getUserDao().getAll();
        Log.d(TAG, "\n\n\n");
        Log.d(TAG, "All users:\n" + all_users.toString());
        return all_users;
    }

    public List<User> searchUser(String name) {
        /**
         *  perform contains search in user table for name
         * @returns list of all user matching the :name if name is empty then will return all
         */
        List<User> all_users;
        if (TextUtils.isEmpty(name)) {
            all_users = dbInstance.getUserDao().getAll();
        } else {
            Log.d(TAG, String.format("Searching User with name %s", name));
            all_users = dbInstance.getUserDao().search(name);
        }
        Log.d(TAG, "\n\n\n");
        Log.d(TAG, "Searched users:\n" + all_users.toString());
        return all_users;
    }

    public void deleteUser(int userId) {
        /**
         *  delete user using userId
         */
        Log.d(TAG, "\n");
        int id = dbInstance.getUserDao().delete(userId);
        if (id > 0) {
            Log.d(TAG, String.format("Deleted User with id %d", userId));
        } else {
            Log.d(TAG, String.format("User not found with id %d", userId));
        }

    }

    public void deleteUser(User user) {
        /**
         *  delete user object from database
         */
        dbInstance.getUserDao().delete(user);
        Log.d(TAG, String.format("Deleted User %s", user.getName()));

    }

    public boolean checkWebsitePermissionByUsername(String name, String website) {
        /**
         * method to check if user has permission for particular website name
         */
        if (TextUtils.isEmpty(name) || TextUtils.isEmpty(website)) {
            Log.d(TAG, "Invalid name or website");
            return false;
        }
        boolean hasAccess = dbInstance.getUserDao().hasAccessToWebsite(name, website);
        Log.d(TAG, String.format("Access status for user : %s on website %s = %s", name, website, hasAccess));
        return hasAccess;

    }

    public boolean checkWebsitePermissionByUsername(String name, int websiteId) {
        /**
         * Method to check if user has permission for particular website id
         */
        if (TextUtils.isEmpty(name) || websiteId < 1) {
            Log.d(TAG, "Invalid name or websiteId");
            return false;
        }
        boolean hasAccess = dbInstance.getUserDao().hasAccessToWebsite(name, websiteId);
        Log.d(TAG, String.format("Access status for user : %s on website %s = %s", name, websiteId, hasAccess));
        return hasAccess;
    }

}